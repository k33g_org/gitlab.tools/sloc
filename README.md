# sloc (Java)

## What

This tool can fetch all projects of all descendant sub groups of a main group. First It generates 3 reports:
- `all_projects.json`
- `all_projects.csv`
- `all_projects.txt`

And then for every project, it will count the **Java** SLOC and generate a json report: `java.summary.<name-of-the-project>.json`

## Setup

Provide the appropriate environment variables in `.gitlab-ci.yml`

```yaml
# -----------------------------------------------
#  Parameters
# -----------------------------------------------
variables:
  GITLAB_TOKEN_ADMIN: "${GITLAB_TOKEN_ADMIN}"
  GITLAB_URL: "https://gitlab.com"
  GROUP_PATH: "k33g_org/samples"
```

## Run

Run the pipeline to start the counting of the **Java** SLOC of all projects, after a moment you'll get a list of json files (as artefacts) like `java.summary.<name-of-the-project>.json`

## Remarks

- If you need to use ssh for cloning, go there https://gitlab.com/k33g_org/gitlab.tools/sloc/-/blob/master/projects-list.js#L206
- If you need to count anything other than **Java**, go there https://gitlab.com/k33g_org/gitlab.tools/sloc/-/blob/master/count.lang.js#L7

## Disclaimer :wave:

These scripts are provided for educational purposes. This project is subject to an opensource license (feel free to fork it, and make it better). You can modify the scripts according to your needs. This project is not part of the GitLab support. However, if you need help do not hesitate to create an issue in the associated project, or better to propose a Merge Request.
:warning: Before using these scripts in production, check the consistency of the results on test instances, and of course make backups.
