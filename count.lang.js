#!/usr/bin/env node

const fs = require("fs")

let rawdata = fs.readFileSync(process.argv[2])
let data = JSON.parse(rawdata).byExt
if(data.java) {
  let report = JSON.stringify(data.java.summary, null, 2)
  console.log(report)
  fs.writeFileSync("./java.summary." + process.argv[2], report)
}
